================
Index Convention
================

Elvet has two possible options to solve a differential equation or minimize a functional: non-batch mode (the default for differential equations) and batch model (the default for functional minimization). Index convention slightly changes between these options. In batch mode, the domain, output and derivatives include all the training points 
in the domain; in non-batch mode, each training example calculated individually. Batch mode can be set in `solver <api/elvet.html#elvet.solver>`_ or in `minimizer <api/elvet.html#elvet.minimizer>`_
simply by passing ``batch=True`` or ``batch=False``.

Non-Batch Mode
--------------

For instance, let us define an equation in the non-batch mode, which takes up to second-order derivative

.. code-block:: python

    def equation(x, y, dy, d2y):
        my_operation = ...
        return my_operation

Assuming a multi-dimensional example for generality where ``domain=elvet.box((0, 1, 10), (0, 1, 10))`` 
and ``model=elvet.nn(2, 10, 2)`` the shapes of the equation inputs would be as follows

.. code-block:: python

    def equation(x,y,dy,d2y):
        assert x.shape == (2,)
        assert y.shape == (2,)
        assert dy.shape == (2, 2)
        assert d2y.shape == (2, 2, 2)

where one can easily access each individual axis as follows

.. code-block:: python

    def equation(x, y, dy, d2y):
        x1, x2 = x[0], x[1]

        dy1_dx1, dy2_dx2 = dy[0, 0], dy[1, 1]

        d2y1_dx1x1, d2y1_dx2x2 = elvet.math.diagonals(d2y, y_index=0, return_list=True, batch=False)
        # similarly: d2y1_dx1x1, d2y1_dx2x2 = d2y[0, 0, 0] , d2y[1, 1, 0]

The index convention of the derivatives follows ``dy_dx = dy[xdim, ydim]``, ``d2y_d2x = d2y[xdim, xdim, ydim]`` and so on
the y dimensionality is expressed as the last index of the given higher order derivative. The `diagonals <api/elvet.math.html#elvet.math.diagonals>`_ function
is simply a helper to extract the diagonal terms on x-axis from any order derivative where if one assumes `Hessian matrix <https://en.wikipedia.org/wiki/Hessian_matrix>`_:

.. math::
    \mathcal{D}^{2}(f_i(\mathbf{x})) = \left( \begin{array}{ccc}
    \frac{\partial^2 f_i(\mathbf{x})}{\partial x_1 \partial x_1} & \dots & \frac{\partial^2 f_i(\mathbf{x})}{\partial x_1 \partial x_N}\\
    \vdots & \ddots & \vdots\\
    \frac{\partial^2 f_i(\mathbf{x})}{\partial x_N \partial x_1} & \dots & \frac{\partial^2 f_i(\mathbf{x})}{\partial x_N \partial x_N}
    \end{array}\right)

which is equal to ``d2y[:, :, i]``. The ``diagonals`` function will extract diagonal terms of this matrix, ``d2y``, for desired y-axis, ``y_index`` which can also 
accessed by typing desired index dimensionality.


Batch Mode
----------

The batch mode intentionally designed especially for integral equations where one needs to be able to see
the full domain to integrate a function (for details see `integral function <api/elvet.math.html#elvet.math.integral>`_)
Since each input of the ``equation`` function now includes all the training examples, tensor shapes are modified as follows

.. code-block:: python

    def equation(x, y, dy, d2y):
        assert x.shape == (100, 2)
        assert y.shape == (100, 2)
        assert dy.shape == (100, 2, 2)
        assert d2y.shape == (100, 2, 2, 2)

hence accessing each axis changes accordingly

.. code-block:: python

    def equation(x, y, dy, d2y):
        x1, x2 = x[0], x[1]

        dy1_dx1, dy2_dx1 = elvet.unstack(dy[:, 0, :])
        assert dy1_dx1.shape == (100, 1)

        dy1_dx1 = dy[:, 0, 0]
        assert dy1_dx1.shape == (100,)

        d2y1_dx1x1, d2y1_dx2x2 = elvet.math.diagonals(d2y, y_index=0, return_list=True, batch=True)
        assert d2y1_dx1x1.shape == (100, 1)
        assert d2y1_dx2x2.shape == (100, 1)

        # second derivative of y1 with respect to x1
        d2y1_dx1x1 = d2y[:, 0, 0, 0]
        assert d2y1_dx1x1.shape == (100,)

        d2y1_dx1x1 = elvet.unstack(d2y[:, 0, 0, :])[0]
        assert d2y1_dx1x1.shape == (100, 1)

In order to preserve dimensionality, `unstack <api/elvet.utils.html#module-elvet.utils.unstack>`_
function has been introduced which splits the first index of the given vector into a list 
while preserving the shape of the array.
