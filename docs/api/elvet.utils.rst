The elvet.utils subpackage
==========================

elvet.utils.LRschedulers module
-------------------------------

.. automodule:: elvet.utils.LRschedulers
    :members:
    :undoc-members:
    :show-inheritance:

elvet.utils.callbacks module
----------------------------

.. automodule:: elvet.utils.callbacks
    :members:
    :undoc-members:
    :show-inheritance:

elvet.utils.loss\_combinators module
------------------------------------

.. automodule:: elvet.utils.loss_combinators
    :members:
    :undoc-members:
    :show-inheritance:

elvet.utils.metrics module
--------------------------

.. automodule:: elvet.utils.metrics
    :members:
    :undoc-members:
    :show-inheritance:

elvet.utils.unstack module
--------------------------

.. automodule:: elvet.utils.unstack
    :members:
    :undoc-members:
    :show-inheritance:
