Index
=====

.. toctree::
   :maxdepth: 1

   installation
   quickstart
   examples
   index_convention
   api/index

.. include:: installation.rst

.. include:: quickstart.rst
