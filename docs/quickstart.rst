===========
Quick start
===========

As a quick example, we can use Elvet to solve the `logistic differential equation <https://en.wikipedia.org/wiki/Logistic_function#Logistic_differential_equation>`_:

.. math::
   \frac{df}{dx} - f(x) (1 - f(x)) = 0, \qquad f(0) = 1/2

We need to::

  import elvet

define the equation as a function taking the values of x, f, and its derivative::

  def equation(x, f, df_dx):
      return df_dx - f * (1 - f)


define the boundary condition::

  bc = elvet.BC(0, lambda x, f, df_dx: f - 1/2)

and the domain over which we want to sove the equation::

  domain = elvet.box((-5, 5, 101))

Finally, we are ready to solve the equation by doing::

  result = elvet.solver(equation, bc, domain, epochs=5e3)

The ``solver`` function generates a ``Solver`` object, which contains a machine learning model. This model is, by default, a fully connected neural net with a 10-unit hidden layer and 1 unit in the input and output layers. The `epochs` argument specifies over how many epochs this model is to be trained.

The predictions of the trained model, which give the solution to the differential equations, can be obtained by calling ``result.prediction()``. We can also plot them, and compare with the analytic solution, which is just the sigmoid function::

  import elvet.plotting

  def analytic_solution(x):
      return 1 / (1 + elvet.math.exp(-x))

  elvet.plotting.plot_prediction(result, true_function=analytic_solution)

This code should produce the plot

.. image :: ../images/logistic_prediction.png
